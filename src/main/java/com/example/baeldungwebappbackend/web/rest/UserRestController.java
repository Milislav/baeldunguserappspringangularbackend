package com.example.baeldungwebappbackend.web.rest;

import com.example.baeldungwebappbackend.enteties.User;
import com.example.baeldungwebappbackend.repository.UserRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/users")
public class UserRestController {

    private final UserRepository userRepository;

    public UserRestController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping()
    public List<User> getAllUsers(){
        return (List<User>) userRepository.findAll();
    }

    @PostMapping("/add")
    public void addUser(@RequestBody User user){
        userRepository.save(user);
    }
}
