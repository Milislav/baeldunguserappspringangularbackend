package com.example.baeldungwebappbackend.repository;

import com.example.baeldungwebappbackend.enteties.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface UserRepository  extends JpaRepository<User, Long> {

}
